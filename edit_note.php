<?php
session_start();
$user = $_SESSION['id'];
session_write_close();
function logout()
{
    unset($_SESSION['id']);
    die(header('Location: index.php'));
}

if (isset($_GET['logout'])) {
    logout();
}
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Notes</title>
</head>
<nav role="navigation" class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Навигация</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="index.php" class="navbar-brand">Notes</a>
    </div>
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="index.php">Главная</a></li>
            <li><a href="view.php">Все заметки</a></li>
            <li class="active"><a href="#">Редактирование</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <h4>Здравствуйте, <?php echo $_SESSION['username']; ?>
                    <small>Ваш ID: <?php echo $_SESSION['id']; ?>  </small>
                </h4>
            </li>
            <li><a href="?logout">Выход</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <?php
    include_once "config.php";
    $link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    if ($link == false) {
        die("Ошибка: " . mysqli_connect_error());
    }
    if (isset($_GET['id'])) {
        $note_id = $_GET['id'];
        $sql = "SELECT owner FROM note WHERE id = '$note_id'";
        $owner = 0;
        if (mysqli_query($link, $sql)) {
            $result = mysqli_query($link, $sql);
            $owner = mysqli_fetch_array($result, 1);
        }
        if ($user === $owner['owner']) {
            if (isset($_POST['label']) && isset($_POST['body'])) {
                $label = mysqli_real_escape_string($link, $_POST['label']);
                $body = mysqli_real_escape_string($link, $_POST['body']);
                $color = mysqli_real_escape_string($link, $_POST['color']);
                if (isset($_POST['tags'])) {
                    $tags = strtolower(mysqli_real_escape_string($link, $_POST['tags']));
                    $tags = explode(",", $tags);

                    /*
                    $tags = array_map('trim', $tags);
                    $sql = "DELETE FROM tags_note WHERE id_tags IN (SELECT id FROM tags WHERE name NOT IN ('". implode("', '", $tags) ."'))"; //удаление связи тега с запиской из бд, если тег был удален при редактировании
                    //потенциальный бред */
                    $sql = "DELETE FROM tags_note WHERE id_note = '$note_id'";

                    //или просто удалить все теги и добавить их заново
                    // или это бред
                    $query = mysqli_query($link, $sql);
                    if (!query) {
                        die(mysqli_error($link));
                    }
                    $sql = "UPDATE note SET label='$label', body='$body', color='$color', last_edit=CURRENT_TIMESTAMP WHERE id = '$note_id'";
                    $query = mysqli_query($link, $sql);
                    if (!query) {
                        die(mysqli_error($link));
                    }
                    foreach ($tags as $tag) {
                        $tag = trim($tag);
                        $result = mysqli_query($link, "SELECT * FROM tags WHERE name='" . $tag . "'");
                        if (mysqli_num_rows($result) > 0) {

                            //тег существует
                            while ($row = mysqli_fetch_assoc($result)) {
                                $last_tag_id = $row['id'];
                                $sql = "INSERT IGNORE INTO tags_note(id_note, id_tags) VALUES ('$note_id', '$last_tag_id')";
                                if (!mysqli_query($link, $sql)) {
                                    echo "Ошибка: " . mysqli_error($link);
                                }
                            }
                        } else {
                            $sql = "INSERT INTO tags (name) VALUES(LOWER('$tag'))";
                            if (mysqli_query($link, $sql)) {
                                $last_tag_id = mysqli_insert_id($link);
                                $sql = "INSERT INTO tags_note(id_note, id_tags) VALUES ('$note_id', '$last_tag_id')";
                                if (!mysqli_query($link, $sql)) {
                                    echo "Ошибка: " . mysqli_error($link);
                                }

                                //echo "Тег " . $tag . " добавлен.\r\n";


                            } else {
                                echo "Ошибка: " . mysqli_error($link);
                            }
                        }
                    }
                    echo "<div class=\"alert alert-success\" role=\"alert\">Обновлено</div>";
                }
            }
            $sql = "SELECT name FROM tags WHERE id IN (SELECT id_tags FROM tags_note WHERE id_note = " . $note_id . ");";
            $tags_array = array();
            if ($tags = mysqli_query($link, $sql)) {
                while ($tags_arr = mysqli_fetch_assoc($tags)) {
                    $tags_array[] = $tags_arr['name'];
                }
            }
            $tags = implode(', ', $tags_array);
            $sql = "SELECT label, body, timestamp, last_edit FROM note WHERE id = '$note_id'";
            if ($result = mysqli_query($link, $sql)) {
                $note = mysqli_fetch_array($result, 1);
                ?>
                <form action="edit_note.php?id=<?php echo $note_id; ?>" method="post" enctype="multipart/form-data"
                      accept-charset="utf-8">
                    <div class="form-group">
                        <label>Дата создания</label>
                        <input class="form-control" type="text" placeholder="<?php echo $note['timestamp']; ?>"
                               readonly>
                    </div>
                    <?php
                    if ($note['last_edit'] != "0000-00-00 00:00:00") {


                        ?>
                        <div class="form-group">
                            <label>Дата последнего редактирования</label>
                            <input class="form-control" type="text" placeholder="<?php echo $note['last_edit']; ?>"
                                   readonly>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="form-group">
                        <label>Заголовок</label>
                        <input class="form-control" type="text" size="50" name="label" id="label"
                               value="<?php echo $note['label']; ?>">
                    </div>
                    <div class="form-group">
                        <label>Цвет заголовка</label>
                        <select name="color" class="form-control">
                            <?php
                            $color = "SELECT * FROM colors";
                            if ($result = mysqli_query($link, $color)) {
                                while ($row = mysqli_fetch_assoc($result)) {
                                    echo "<option value=\"" . $row['color_id'] . "\" style=\"background-color: #" . $row['color_code'] . "\">". $row['color_name'] ."</option>";
                                }
                            } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Текст</label>
                        <textarea class="form-control" rows="20" cols="100" name="body"
                                  id="body"><?php echo $note['body']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Теги</label>
                        <input class="form-control" type="text" size="50" name="tags" id="tags"
                               value="<?php echo $tags; ?>">
                    </div>
                    <button type="submit" class="btn btn-default">Обновить</button>
                </form>
            <?php
            } else {
                die(mysqli_error($link));
            }
        } else {
            echo "<div class=\"alert alert-danger\" role=\"alert\">Вам нельзя этого делать</div>";
        }
    }
    ?>
</div>