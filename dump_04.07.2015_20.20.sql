CREATE DATABASE  IF NOT EXISTS `notes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `notes`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: notes
-- ------------------------------------------------------
-- Server version	5.5.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_code` varchar(6) NOT NULL DEFAULT '000000',
  `color_name` varchar(30) NOT NULL DEFAULT 'серобуромалиновый',
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'000000','Черный'),(2,'0000FF','Синий'),(3,'FF0000','Красный'),(4,'FFFF00','Желтый'),(5,'008000','Зеленый'),(6,'FF00FF','Фиолетовый');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `note_id` int(10) unsigned NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_size` varchar(200) NOT NULL,
  `file_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (68,17,132,'1436030211_jWp40IR.png','2440','image/png');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(200) DEFAULT NULL,
  `body` mediumtext,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `color` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `last_edit` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
INSERT INTO `note` VALUES (132,'Lorem Ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer faucibus, est quis pharetra tempus, justo est commodo nulla, et venenatis augue dui eget leo. Suspendisse sit amet posuere est. Proin quis nisi a eros pretium pharetra tincidunt at nulla. Morbi sed cursus tellus. Vivamus molestie ut dolor eget mollis. Phasellus suscipit gravida risus quis tempor. Aliquam quis odio volutpat, efficitur nunc et, blandit mauris.\r\n\r\nMorbi auctor, lectus sit amet feugiat ullamcorper, tellus lorem vestibulum urna, eu mollis nisi odio sed augue. Curabitur accumsan neque sed quam sodales euismod. Nulla turpis diam, rhoncus vitae elit nec, pretium maximus nibh. Etiam id interdum erat, ut molestie turpis. Aliquam lobortis turpis mauris, vel eleifend sapien dictum ut. Proin in ultricies libero, ac dignissim felis. Duis a mollis risus. Cras tempus libero semper, dapibus odio id, viverra felis. Vivamus sed magna sed felis pretium cursus. Phasellus molestie quam ac lacus condimentum gravida. Quisque sit amet nunc tincidunt, tincidunt quam id, fermentum purus. Sed lobortis eleifend arcu, sed tincidunt lacus tincidunt vitae. Proin posuere, purus nec commodo placerat, neque lacus viverra eros, a lacinia nisi lacus nec lorem. In ornare enim ac libero tempus scelerisque. Aliquam auctor est mi, ut facilisis lectus rutrum nec.\r\n\r\nVestibulum a turpis commodo lorem ornare lobortis et quis tortor. Aliquam semper quam viverra nibh ultricies, vel sollicitudin lacus euismod. Sed a tellus nec nisl condimentum mattis. Curabitur dignissim eu nisl id dictum. Sed facilisis auctor elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n\r\nFusce est ligula, tincidunt eget eros vitae, faucibus pulvinar arcu. Donec luctus accumsan scelerisque. Aliquam eu lorem nisl. Morbi auctor suscipit nunc vel consectetur. Donec aliquam, lacus et pretium rhoncus, augue dolor faucibus augue, ut aliquet diam augue quis lacus. Vivamus suscipit a felis ut consequat. In ultricies volutpat nulla at euismod. Sed pretium turpis lorem, et imperdiet erat laoreet id. Nunc sed mauris eros. Aenean auctor, dui non luctus sodales, ex mi pulvinar ex, in aliquet ipsum mauris a lectus.\r\n\r\nDonec sit amet gravida nulla, at pretium mauris. Sed neque urna, bibendum nec odio a, auctor rhoncus sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer mi leo, pellentesque eu odio id, scelerisque ornare felis. Aenean nulla neque, tempus eu quam aliquet, aliquet interdum nisl. Phasellus odio libero, posuere et dignissim non, tincidunt eu magna. Aliquam erat volutpat. Maecenas cursus, magna sit amet tempor efficitur, ipsum nulla suscipit erat, non facilisis nulla purus id arcu. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur porta ex magna, sit amet gravida lacus ultricies sit amet.','2015-07-04 17:16:51',1,17,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_tag_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (85,'lorem ipsum'),(86,'lorem'),(87,'ipsum');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_note`
--

DROP TABLE IF EXISTS `tags_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_note` (
  `id_note` int(11) NOT NULL,
  `id_tags` int(11) NOT NULL,
  PRIMARY KEY (`id_note`,`id_tags`),
  KEY `tag_id_idx` (`id_tags`),
  CONSTRAINT `note_id` FOREIGN KEY (`id_note`) REFERENCES `note` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tag_id` FOREIGN KEY (`id_tags`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_note`
--

LOCK TABLES `tags_note` WRITE;
/*!40000 ALTER TABLE `tags_note` DISABLE KEYS */;
INSERT INTO `tags_note` VALUES (132,85),(132,86),(132,87);
/*!40000 ALTER TABLE `tags_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_note`
--

DROP TABLE IF EXISTS `user_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_note` (
  `id_user` int(11) NOT NULL,
  `id_note` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_note`),
  KEY `fk_note_id_idx` (`id_note`),
  CONSTRAINT `fk_note_id` FOREIGN KEY (`id_note`) REFERENCES `note` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_note`
--

LOCK TABLES `user_note` WRITE;
/*!40000 ALTER TABLE `user_note` DISABLE KEYS */;
INSERT INTO `user_note` VALUES (17,132),(18,132);
/*!40000 ALTER TABLE `user_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (17,'admin','41eb409b42cb75355cca6634dbf69cd6883f92af','420b9b66699df8d232f473d78ce195f8bd06a0b9bce457889abd02ccf61f867363fbee0969e5081d6a5c5d24d03333eb58690163073444a8aba6c886a34a2e39'),(18,'test','6331fc73216ba4c5c94d45b176027ebd90d06ed4','b5d6c24deefab17ef2ef9d8706f6bc65ad6bc02bdd8c602efa491f92012a838b6813fb1bbb101e178f355f1e9f4990979866689ea559029d508d8f5a38c52151');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-04 20:20:40
