<?php
session_start();
$user = $_SESSION['id'];
function logout()
{
    unset($_SESSION['id']);
    die(header('Location: index.php'));
}
if (isset($_GET['logout'])) {
    logout();
}
?>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>Notes</title>
    </head>
    <nav role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand">Notes</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Главная</a></li>
                <li><a href="view.php">Все заметки</a></li>
                <li class="active"><a href="#">Пользователи</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <h4>Здравствуйте, <?php echo $_SESSION['username']; ?>
                        <small>Ваш ID: <?php echo $_SESSION['id']; ?>  </small>
                    </h4>
                </li>
                <li><a href="?logout">Выход</a></li>
            </ul>
        </div>
    </nav>
<body>
    <div class="container">
<?php
include_once('config.php');
$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if ($link == false) {
    die("ERROR: " . mysqli_connect_error());
}
if (isset($_GET{'id'})) {
    $note_id = $_GET['id'];
    $sql = "SELECT owner FROM note WHERE id = '$note_id'";
    $owner = 0;
    if (mysqli_query($link, $sql)) {
        $result = mysqli_query($link, $sql);
        $owner = mysqli_fetch_array($result, 1);
    }
    if ($user === $owner['owner']) {
        if ((isset($_GET['action']) && $_GET['action'] == "delete") && isset($_GET['user_id'])) {
            $user_id = $_GET['user_id'];
            $sql = "DELETE FROM user_note WHERE id_note = '$note_id' AND  id_user = '$user_id'";
            $query = mysqli_query($link, $sql);
            if (!query) {
                die(mysqli_error($link));
            }
            echo "<div class=\"alert alert-success\" role=\"alert\">Обновлено</div>";
        }
        if ((isset($_GET['action']) && $_GET['action'] == "add") && isset($_GET['user_login'])) {
            $user_login = mysqli_real_escape_string($link,$_GET['user_login']);
            $sql = "SELECT id FROM users WHERE username = '$user_login'";
            if (mysqli_query($link, $sql)) {
                $query = mysqli_query($link, $sql);
                $numrows = mysqli_num_rows($query);
                if ($numrows === 1) {
                    $user_id = mysqli_fetch_array($query, 1);
                    $userid = $user_id['id'];
                    $sql = "INSERT INTO user_note (id_user, id_note) VALUES ('$userid', '$note_id')";
                    $query = mysqli_query($link, $sql);
                    if (!query) {
                        die(mysqli_error($link));
                    }

                } else {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Пользователей с таким логином нет</div>";
                }
            }
            echo "<div class=\"alert alert-success\" role=\"alert\">Обновлено</div>";
/*
            $sql = " user_note WHERE id_note = '$note_id' AND  id_user = '$user_id'";
            $query = mysqli_query($link, $sql);
            if (!query) {
                die(mysqli_error($link));
            }*/
        }
        echo "<table class=\"table table-bordered table-hover\"><tr class=\"success\"><td>Логин</td><td width=\"20%\">Опции</td></tr>";
        $login = "SELECT username, id FROM users WHERE id IN (SELECT id_user FROM user_note WHERE id_note = " . $note_id . ");";
        $users_array = array();
        if ($users = mysqli_query($link, $login)) {
            while ($row = mysqli_fetch_assoc($users)) {
                $users_array[] = $row;
            }
        }
        if (!empty($users_array)) {
            foreach ($users_array as $user_info) {
                if ($user_info['id'] != $user) {
                    echo "<tr><td>" . $user_info['username'] . "</td><td><a href = \"users_note.php?id=" . $note_id . "&action=delete&user_id=" . $user_info['id'] . "\" onClick=\"return confirm('Точно удалить?')\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a></td></tr>";
                }
            }
        }
        ?>
        <form action="users_note.php" method="get" accept-charset="utf-8">
            <div class="form-group">
                <label>Логин</label>
        <input class="form-control" type="text" name="user_login" id="user_login" placeholder="Логин" autocomplete="off" autofocus>
        <input type="hidden" name="action" value="add">
        <input type="hidden" name="id" id="id" value="<?php echo $note_id; ?>"><br/>
            <button type="submit" class="btn btn-default">Добавить</button>
                </div>
        </form>


    <?php
    } else {
        echo "<div class=\"alert alert-danger\" role=\"alert\">Вам нельзя этого делать</div>";
    }

}

?>
    </div>