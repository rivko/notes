<!DOCTYPE html>
<html lang="en">
<head>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Notes</title>
</head>
<body>
<div class="container">
    <h1>Регистрация</h1>

    <form action="register.php" id="registerform" method="post" name="registerform">
        <div class="form-group">
            <label>Логин</label>
            <input  class="form-control"  id="username" name="username" size="20" placeholder="Логин" type="text" value=""
                   required></div>
        <div class="form-group">
            <label>Пароль</label>
            <input  class="form-control"  id="password" name="password" size="32" placeholder="Пароль" type="password" value=""
                   required></div>
        <button type="submit" class="btn btn-default" name="register">Зарегистрироваться</button>
        или <a href="index.php">войти</a>
    </form>
    <?php
    include_once "config.php";

    $link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if ($link == false) {
        die("ERROR: " . mysqli_connect_error());
    }
    if (isset($_POST["register"])) {

        if (!empty($_POST['username']) || !empty($_POST['password'])) {
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
            $query = mysqli_query($link, "SELECT * FROM users WHERE username='" . $username . "'");
            if (!query) {
                die(mysqli_error($link));
            }
            $numrows = mysqli_num_rows($query);
            if ($numrows == 0) {
                $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
                $password = sha1(md5($password) . $random_salt);
                $sql = "INSERT INTO users
  (username, password, salt)
	VALUES('$username', '$password', '$random_salt')";
                $result = mysqli_query($link, $sql);
                if ($result) {
                    echo "<br/><div class=\"alert alert-success\" role=\"alert\">Аккаунт создан</div>";
                } else {
                    echo "<br/><div class=\"alert alert-danger\" role=\"alert\">Ошибка при создании</div>";
                }
            } else {
                echo "<br/><div class=\"alert alert-danger\" role=\"alert\">Такой логин уже занят</div>";
            }
        } else {
            echo "<br/><div class=\"alert alert-warning\" role=\"alert\">Заполните все поля</div>";
        }
    }
    ?>
</div>
</body>
</html>