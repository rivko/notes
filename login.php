<?php
include_once "config.php";
$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if ($link == false) {
    die("ERROR: " . mysqli_connect_error());
}
function logout()
{
    unset($_SESSION['id']);
    die(header('Location: ' . $_SERVER['PHP_SELF']));
}
function login($username, $password, $link) {
    $sql = "SELECT * FROM users WHERE `username`='$username';";
    if (mysqli_query($link, $sql)) {
        $result = mysqli_query($link, $sql);
        $USER = mysqli_fetch_array($result, 1);
        if (!empty($USER)) {
            $password = sha1(md5($password) . $USER['salt']);
            if ($USER['password'] == $password) {
                $_SESSION = array_merge($_SESSION, $USER);
                return true;
            } 
            else {
                return false;
            }
        } 
        else {
            return false;
        }
    } 
    else {
        echo "Error: " . mysqli_error($link);
    }
    return false;
}
if (isset($_SESSION['id'])) {
    define('USER_LOGGED', true);
    $UserName = $_SESSION['username'];
    $UserPass = $_SESSION['password'];
    $UserID = $_SESSION['id'];
} 
else {
    define('USER_LOGGED', false);
}
if (isset($_POST['login'])) {
    
    if (get_magic_quotes_gpc()) {
        $_POST['user'] = stripslashes($_POST['user']);
        $_POST['pass'] = stripslashes($_POST['pass']);
    }
    $user = mysqli_real_escape_string($link, $_POST['user']);
    $pass = mysqli_real_escape_string($link, $_POST['pass']);
    if (login($user, $pass, $link)) {
        header('Refresh: 1');
        echo "<meta charset=\"utf-8\">";
        die('Вы успешно вошли');
    } 
    else {
        header('Refresh: 2;');
        echo "<meta charset=\"utf-8\">";
        die('Неверный пароль');
    }
}
if (isset($_GET['logout'])) {
    logout();
}
?>