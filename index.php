<?php
session_start();

include('login.php');

?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
        <script src="js/bootstrap.js"></script>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>Notes</title>
    </head>
    <script src="js/bootstrap.min.js"></script>
<?php
if (USER_LOGGED) {
    ?>

    <nav role="navigation" class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand">Notes</a>
        </div>
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Главная</a></li>
                <li><a href="view.php">Все заметки</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <h4>Здравствуйте, <?php echo $UserName; ?>
                        <small>Ваш ID: <?php echo $UserID; ?>  </small>
                    </h4>
                </li>
                <li><a href="?logout">Выход</a></li>
            </ul>
        </div>
    </nav>
    <html>

    <body>
    <div class="container">

        <form action="add_note.php" method="post" enctype="multipart/form-data" accept-charset="utf-8">
            <div class="form-group">
                <label for="label">Заголовок</label>
                <input class="form-control" type="text" name="label" id="label" placeholder="Заголовок">
            </div>
            <div class="form-group">
                <label>Цвет заголовка</label>
                <select name="color" class="form-control">
                    <?php
                    include_once('config.php');
                    $link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
                    if ($link == false) {
                        die("Ошибка: " . mysqli_connect_error());
                    }
                    $color = "SELECT * FROM colors";
                    if ($result = mysqli_query($link, $color)) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<option value=\"" . $row['color_id'] . "\" style=\"background-color: #" . $row['color_code'] . "\">" . $row['color_name'] . "</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Текст</label>
                <textarea class="form-control" rows="10" cols="60" name="body" id="body" placeholder="Текст"></textarea>
            </div>
            <div class="form-group">
                <label>Теги</label>
                <input class="form-control" type="text" name="tags" id="tags" size="60"
                       placeholder="Теги (через запятую)">
            </div>
            <div class="form-group">
                <label>Файлы</label>
                <input type="file" name="files[]" multiple=""/>Разрешены только gif png jpg avi mp4 doc docx odt
                ppt и не больше 200 МБ<br/>
            </div>
            <button type="submit" class="btn btn-default">Добавить</button>
        </form>
    </div>
    </body>
    </html>
<?php
} else { ?>
    <div class="container">
        <form method="POST" action="<?php
        echo $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
                <label>Логин</label>
                <input class="form-control" type="text" name="user" placeholder="Логин">
            </div>
            <div class="form-group"><label>Пароль</label>
                <input type="password" class="form-control" name="pass" placeholder="Пароль">
            </div>
            <button type="submit" name="login" class="btn btn-default">Войти</button>
        </form> или <a href="register.php">зарегистрироваться</a></p>
    </div>
<?php
}
?>