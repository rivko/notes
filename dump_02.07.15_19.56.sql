CREATE DATABASE  IF NOT EXISTS `notes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `notes`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: notes
-- ------------------------------------------------------
-- Server version	5.5.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_code` varchar(6) NOT NULL DEFAULT '000000',
  `color_name` varchar(30) NOT NULL DEFAULT 'серобуромалиновый',
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'FF0000','Красный'),(2,'0000FF','Синий'),(3,'000000','Черный'),(4,'FFFF00','Желтый'),(5,'008000','Зеленый'),(6,'FF00FF','Фиолетовый');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `note_id` int(10) unsigned NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_size` varchar(200) NOT NULL,
  `file_type` varchar(200) NOT NULL,
  `color` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (33,14,72,'1435749491_[rutracker.org].t3689864.torrent','98572','application/x-bittorrent',0),(47,13,109,'1435778605_3bj8tPD.png','2169','image/png',0),(49,13,111,'1435782516_3bj8tPD.png','2169','image/png',0),(50,13,111,'1435782516_8jl3r7p.png','8023','image/png',0),(51,13,111,'1435782516_56qRTDa.png','33068','image/png',0),(52,14,114,'1435844081_3bj8tPD.png','2169','image/png',0),(53,14,115,'1435848930_18004.jpg','271506','image/jpeg',0);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(200) DEFAULT NULL,
  `body` mediumtext,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `color` int(11) NOT NULL DEFAULT '3',
  `owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
INSERT INTO `note` VALUES (111,'3 картинки','123','2015-07-01 20:28:36',1,13),(113,'123','123','2015-07-02 12:45:35',1,14),(114,'123','123','2015-07-02 13:34:41',1,14),(115,'123','kek','2015-07-02 14:55:30',5,14);
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_tag_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (39,'test'),(40,'test2'),(41,'3'),(42,'1'),(43,'расрас'),(44,'два'),(45,'три'),(46,'ЧЕТЫРЕ'),(47,'tag1'),(48,'пять'),(49,'kek'),(50,'kak'),(51,''),(52,'рас'),(53,'pac'),(54,'notepad.exe'),(55,'notepad'),(56,'блокнот'),(57,'123'),(58,'321'),(59,'4'),(60,'asdsad'),(61,'23'),(62,'архив'),(63,'торрент'),(64,'картинка'),(65,'Тег1'),(66,'Тег2'),(67,'Тег3'),(68,'time'),(69,'timer'),(70,'pit'),(71,'pmtimer'),(72,'hpet'),(73,'tsc'),(74,'rdtsc'),(75,'rtc'),(76,'13232');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_note`
--

DROP TABLE IF EXISTS `tags_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_note` (
  `id_note` int(11) NOT NULL,
  `id_tags` int(11) NOT NULL,
  PRIMARY KEY (`id_note`,`id_tags`),
  KEY `tag_id_idx` (`id_tags`),
  CONSTRAINT `note_id` FOREIGN KEY (`id_note`) REFERENCES `note` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tag_id` FOREIGN KEY (`id_tags`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_note`
--

LOCK TABLES `tags_note` WRITE;
/*!40000 ALTER TABLE `tags_note` DISABLE KEYS */;
INSERT INTO `tags_note` VALUES (111,57),(113,57),(114,57),(115,65),(115,66);
/*!40000 ALTER TABLE `tags_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_note`
--

DROP TABLE IF EXISTS `user_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_note` (
  `id_user` int(11) NOT NULL,
  `id_note` int(11) NOT NULL,
  PRIMARY KEY (`id_user`,`id_note`),
  KEY `fk_note_id_idx` (`id_note`),
  CONSTRAINT `fk_note_id` FOREIGN KEY (`id_note`) REFERENCES `note` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_note`
--

LOCK TABLES `user_note` WRITE;
/*!40000 ALTER TABLE `user_note` DISABLE KEYS */;
INSERT INTO `user_note` VALUES (13,111),(14,111),(14,113),(14,114),(14,115);
/*!40000 ALTER TABLE `user_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (13,'test2','187098b9e1060c35dbe1ce909147a35b39aa683e','417179cdedfe3baf82fc52a107d9b80ce7d0ec6de255d6c87637c7bc42679e18152a0681dfa8ac7cb630a34ad1399387d049e9326090b064a6da02e0f14af4d4'),(14,'admin','dc1cf2110b5905a0e5ba40730463a919d2107957','da3989873d344dd199149eeeded32f0c13ce15bc3df982fe8feca43d314892716907faee17c3a55a5ac6ae91ed9d2722d2dc2092b0109cbed4bf2d53589ced7a'),(15,'kek','49e708485a23ffe94810aa92bd5e42304ce891a3','bb3800b901bde42cb033687e20b8ef5fc317e354e2c369e64d19e1811c36fbac10b885a50910536295b49b27f5a641b418a29f18e1dfa6722caf72d749e89267'),(16,'lol','47a4dbbe3f2c5fab001cdaa75a27a49b5a8c087e','1d7fe02df013f0f37a31822885fced94ea823dc43e123728f288f167d655563a862a5b62d4b73be5653b105c980ae40b5eda8d81b88fabef419f45129a61eb64');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-02 19:56:33
