<?php
session_start();
$user = $_SESSION['id'];
function logout()
{
    unset($_SESSION['id']);
    die(header('Location: index.php'));
}
if (isset($_GET['logout'])) {
    logout();
}
?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Notes</title>
</head>
<nav role="navigation" class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Навигация</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="index.php" class="navbar-brand">Notes</a>
    </div>
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li><a href="index.php">Главная</a></li>
            <li class="active"><a href="view.php">Все заметки</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>
                <h4>Здравствуйте, <?php echo $_SESSION['username']; ?>
                    <small>Ваш ID: <?php echo $_SESSION['id']; ?>  </small>
                </h4>
            </li>
            <li><a href="?logout">Выход</a></li>
        </ul>
    </div>
</nav>
<body>
<?php
include_once('config.php');
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array(' Б', ' КБ', ' МБ', ' ГБ', ' ТБ');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
$sql = "SELECT * FROM note WHERE id IN (SELECT id_note FROM user_note WHERE id_user = " . $_SESSION['id'] . ")";
echo "<div class=\"container-fluid\"><table width=\"100%\" class=\"table table-bordered table-hover\"><strong><tr class=\"success\"><td>Заголовок</td><td class=\"col-md-4\">Текст</td><td>Теги</td><td>Владелец</td><td class=\"col-md-2\">Файлы</td><td class=\"col-md-2\">Опции</td></tr></strong>";
if ($result = mysqli_query($link, $sql)) {
    while ($row = mysqli_fetch_assoc($result)) {
        $note = $row['id'];
        $color = "SELECT color_code FROM colors WHERE color_id = (SELECT color FROM note WHERE id = '$note')";
        $query = mysqli_query($link, $color);
        if (!query) {
            die(mysqli_error($link));
        }
        $color = mysqli_fetch_array($query, 1);
        echo "<tr><td><font color=\"#" . $color['color_code'] . "\">" . $row['label'] . "</font></td><td><div style=\"max-height: 250px; overflow:auto;white-space: pre-wrap;\">" . $row['body'] . "</div></td><td>";

        $tag = "SELECT name FROM tags WHERE id IN (SELECT id_tags FROM tags_note WHERE id_note = " . $note . ");";
        $tags_array = array();
        if ($tags = mysqli_query($link, $tag)) {
            while ($tags_arr = mysqli_fetch_assoc($tags)) {
                $tags_array[] = $tags_arr['name'];
            }
        }
        echo implode(', ', $tags_array) . "</td><td>";

        $login = "SELECT username FROM users WHERE id = (SELECT owner FROM note WHERE id = " . $note . ");";
        $query = mysqli_query($link, $login);
        if (!query) {
            die(mysqli_error($link));
        }
        $owner = mysqli_fetch_array($query, 1);
        echo $owner['username']."</td><td>";

        $file = "SELECT id, file_name, file_size FROM files WHERE note_id = " . $note;
        $files_array = array();
        if ($files = mysqli_query($link, $file)) {
            while ($file = mysqli_fetch_assoc($files)) {
                $files_array[] = $file;
            }
        }
        if (!empty($files_array)) {
            foreach ($files_array as $file_info) {
                $file_name = substr($file_info['file_name'], 11);
                echo "<a href = \"download.php?id=" . $file_info['id'] . "\"><span class=\"glyphicon glyphicon-download\" aria-hidden=\"true\"></span> " . $file_name . " (" . formatBytes($file_info['file_size']) . ")</a><br/>";
            }
        }

        if ($user == $row['owner']) {
            echo "</td><td><a href = \"edit_note.php?id=" . $note . "\"><span title=\"Редактировать\" class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span></a> <a href = \"delete_note.php?id=" . $note . "\" onClick=\"return confirm('Удалить заметку?')\"><span title=\"Удалить\" class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a> <a href = \"users_note.php?id=" . $note . "\"><span title=\"Поделиться\" class=\"glyphicon glyphicon-share\" aria-hidden=\"true\"></span></a></td></tr>";
        } else {
            echo "</td><td><a href = \"delete_note.php?id=" . $note . "\" onClick=\"return confirm('Развидеть заметку?')\"><span title=\"Удалить\" class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a></td></tr>";
        }


    }
}
echo "</table></div>";
?>
</body>
</html>