<?php
session_start();
include_once "config.php";
$user = $_SESSION['id'];
$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if ($link == false) {
    die("Ошибка: " . mysqli_connect_error());
}

$label = mysqli_real_escape_string($link, $_POST['label']);
$body = mysqli_real_escape_string($link, $_POST['body']);
$tags = strtolower(mysqli_real_escape_string($link, $_POST['tags']));
$color = mysqli_real_escape_string($link, $_POST['color']);

$tags = explode(",", $tags);

$sql = "INSERT INTO note (label, body, color, owner) VALUES ('$label', '$body', '$color', '$user')";
$last_note_id = 0;
if (mysqli_query($link, $sql)) {
    $last_note_id = mysqli_insert_id($link);
} 
else {
    echo "Ошибка: " . mysqli_error($link);
}
$sql = "INSERT INTO user_note(id_user, id_note) VALUES ('$user', '$last_note_id')";
if (mysqli_query($link, $sql)) {
} 
else {
    echo "Ошибка: " . mysqli_error($link);
}
foreach ($tags as $tag) {
    $tag = trim($tag);
    $result = mysqli_query($link, "SELECT * FROM tags WHERE name='" . $tag . "'");
    if (mysqli_num_rows($result) > 0) {
        
        //echo "Тег " . $tag . " уже существует.\r\n";
        while ($row = mysqli_fetch_assoc($result)) {
            $last_tag_id = $row['id'];
            $sql = "INSERT INTO tags_note(id_note, id_tags) VALUES ('$last_note_id', '$last_tag_id')";
            if (!mysqli_query($link, $sql)) {
                echo "Ошибка: " . mysqli_error($link);
            }
        }
    } 
    else {
        $sql = "INSERT INTO tags (name) VALUES('$tag')";
        if (mysqli_query($link, $sql)) {
            $last_tag_id = mysqli_insert_id($link);
            $sql = "INSERT INTO tags_note(id_note, id_tags) VALUES ('$last_note_id', '$last_tag_id')";
            if (!mysqli_query($link, $sql)) {
                echo "Ошибка: " . mysqli_error($link);
            }
            
            //echo "Тег " . $tag . " добавлен.\r\n";
            
            
        } 
        else {
            echo "Ошибка: " . mysqli_error($link);
        }
    }
}
if (isset($_FILES['files'])) {
    $errors = array();
    if ($_FILES['files']['size'][0] != 0) { //зачем-то отдает пустой массив если файлов нет
        foreach ($_FILES['files']['tmp_name'] as $key => $tmp_name) {
            $file_name = time() . "_" . $_FILES['files']['name'][$key];
            $file_size = $_FILES['files']['size'][$key];
            $file_tmp = $_FILES['files']['tmp_name'][$key];
            $file_type = $_FILES['files']['type'][$key];
            $allowed =  array('gif','png' ,'jpg', 'avi', 'mp4', 'doc', 'docx', 'odt', 'ppt');
            $ext = pathinfo($_FILES['files']['name'][$key], PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
    			echo "Файл ".$_FILES['files']['name'][$key]. " не загружен. Разрешены только gif png jpg avi mp4 doc docx odt ppt<br/>";
    			continue;
			}
            if ($file_size > 200 * 1024 * 1024) { //просто так
                die("Размер должен быть меньше 200 МБ");
            }
            $login = "SELECT username FROM users WHERE id = '$user';";
            $query = mysqli_query($link, $login);
            if (!query) {
                die(mysqli_error($link));
            }
            $owner = mysqli_fetch_array($query, 1);
            $query = "INSERT INTO files (user_id, note_id, file_name, file_size, file_type) VALUES ('$user', '$last_note_id', '$file_name','$file_size','$file_type'); ";
            $desired_dir = "uploads";
            if (empty($errors) == true) {
                if (is_dir($desired_dir) == false) {
                    mkdir("$desired_dir", 0700);
                }
                if (is_dir("$desired_dir/" . $owner['username']  . "/". $file_name) == false) {
                    mkdir("uploads/" . $owner['username']  . "/", 0700);
                    move_uploaded_file($file_tmp, "uploads/" . $owner['username']  . "/". $file_name);
                } 
                else {
                    $new_dir = "uploads/" . $file_name . time();
                    rename($file_tmp, $new_dir);
                }
                if (!mysqli_query($link, $query)) {
                    $errors[] = "Ошибка: " . mysqli_error($link) . ". Запрос: " . $query;
                }
            } 
            else {
                
                //echo "<p>Все плохо :/</p>";
                //print_r($errors);
                
            }
        }
    }
    if (empty($error)) {
        echo "<p>Все хорошо.\n</p>";
    }
}
echo '<script>setTimeout(\'window.location.href="view.php"\', 1000);</script>';
mysqli_close($link);
?>