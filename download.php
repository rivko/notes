<?php
session_start();
$user = $_SESSION['id'];
session_write_close();
include_once "config.php";
$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if ($link == false) {
    die("Ошибка: " . mysqli_connect_error());
}
if (isset($_GET['id'])) {
    $upload_dir = "uploads";
    $file_id = $_GET['id'];
    $sql = "SELECT note_id FROM files WHERE id = '$file_id'";
    $query = mysqli_query($link, $sql);
    if (!query) {
        die(mysqli_error($link));
    }
    $note = mysqli_fetch_array($query, 1);
    $sql = "SELECT id_user FROM user_note WHERE id_note = ". $note['note_id'];
    $users_array = array();
    if ($users = mysqli_query($link, $sql)) {
        while ($usr = mysqli_fetch_assoc($users)) {
            $users_array[] = $usr;
        }
    }
    $login = "SELECT username FROM users WHERE id = (SELECT owner FROM note WHERE id = " . $note['note_id'] . ");";
    $query = mysqli_query($link, $login);
    if (!query) {
        die(mysqli_error($link));
    }
    $owner = mysqli_fetch_array($query, 1);
    foreach ($users_array as $user_allowed) {
        if ($user === $user_allowed['id_user']) {
            $sql = "SELECT file_name, file_size, file_type FROM files WHERE id = '$file_id'";
            if ($result = mysqli_query($link, $sql)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $file_name = $row['file_name'];
                    $full_path = $upload_dir . "/". $owner['username']  . "/" . $file_name;
                    $file_size = $row['file_size'];
                    $file_type = $row['file_type'];
                    
                    //echo $full_path;
                    if (!file_exists($full_path)) {
                        header("HTTP/1.0 404 Not Found");
                        return;
                    }
                    header('Content-Description: File Transfer');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header("Contetn-length: " . $file_size);
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . substr($file_name, 11) . '"');
                    readfile($full_path);
                }
            } 
            else {
                die(mysqli_error($link));
            }
        } 
        else {
            //echo "You are not prepared"; //боже
        }
    }
} else {
	echo "Вам нельзя этого делать";
} 
?>
