<?php
session_start();
$user = $_SESSION['id'];
session_write_close();
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.1.3.js"></script>
    <script src="js/bootstrap.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Notes</title>
</head>
<?php
include_once "config.php";
$link = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
if ($link == false) {
    die("Ошибка: " . mysqli_connect_error());
}
if (isset($_GET['id'])) {
    $note_id = $_GET['id'];
    $sql = "SELECT owner FROM note WHERE id = '$note_id'";
    $owner = 0;
    if (mysqli_query($link, $sql)) {
        $result = mysqli_query($link, $sql);
        $owner = mysqli_fetch_array($result, 1);
    }
    if ($user === $owner['owner']) {
        $sql = "DELETE FROM note WHERE id = '$note_id'";
        $query = mysqli_query($link, $sql);
        if (!query) {
            die(mysqli_error($link));
        }
        $sql = "DELETE FROM files WHERE note_id = '$note_id'";
        $query = mysqli_query($link, $sql);
        if (!query) {
            die(mysqli_error($link));
        }
    } else {
        //echo "<div class=\"container\"><div class=\"alert alert-danger\" role=\"alert\">Вам нельзя этого делать</div></div>";
        $sql = "SELECT id_user FROM user_note WHERE id_note = '$note_id'";
        $users_array = array();
        if ($users = mysqli_query($link, $sql)) {
            while ($usr = mysqli_fetch_assoc($users)) {
                $users_array[] = $usr;
            }
        }
        foreach ($users_array as $user_allowed) {
            if ($user_allowed['id_user'] == $user) {
                $sql = "DELETE FROM user_note WHERE id_note = '$note_id' AND id_user='$user'";
                $query = mysqli_query($link, $sql);
                if (!query) {
                    die(mysqli_error($link));
                }
            }
        }
    }
}
echo '<script>window.location.href = "view.php";</script>';
?>